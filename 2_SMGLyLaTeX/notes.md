## LaTeX

> Puede utilizar plantillas
> se utiliza en libros, curriculums, examenes, articulos...
> LaTeX es una adaptacion de Tex (mas corta)
> utiliza una serie de "estilos"
> los ficheros.tex son conversibles a .PDF, .DVI y .PS
> DVI : Gostscript, default en linux, o conversor DVI a PS
> PS  : Formato para impresoras

### Estructura
- Preámbulo
  - \documentclass[estilo]{book, report, article, letter}
  - \usepackage{paquete} Ej:amsmath, amsfonts, amssymb, graghicx, color
  - \title (opcional)
  - \autor (opcional)
  - \date  (opcional)
- Cuerpo
  - \begin{document}
  - \maketitle para crear una portada
  - \end{document}

###tipos para include
  Ej: \include{cap1}
  \part (solo en book)
  \chapter (solo en book y report)
  \section
  \subsection
  \paragraph
  \subparagraph

los comentarios son con %
con \\ saltamos de linea
con \newpage forzamos el salto de pagina

## Medidas

### Medidas absolutas

pt -> 0.35mm
pc -> 12pt
in -> 2.54cm

## Medidas relativas

em -> Tamaño letra M
ex -> tamaño letra X

