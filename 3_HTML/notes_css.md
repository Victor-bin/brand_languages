# CSS

## Niveles

***A nivel de parrafo***
```html
<p style="color: red;">
```

***A nivel de etiqueta***
```css
p {
    color: red;
}
```
***A nivel de id***
```css
#id {
    color: red;
}
```
***A nivel de clase ***
```css
.class {
    color: red;
}
```