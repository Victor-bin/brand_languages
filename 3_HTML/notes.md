# HTML
## versiones html
- html 4.01( extricto, transicional y con marcos )
- xhtml
- html 5

## Entornos web
- Lenguaje de marcas (HTML)
- Protocolo (HTTP)
- Localizador (URL)

## Documento
- Texto simple
- Imagen/videos
- Enlaces(locales o remotos)

## Acceso
- Navegador
- Servidor Web

## caracteristicas HTML
- Estandarizado
- Multiplataforma
- Lenguajes de marcas
- Debe ser interpretado (navegador)
- Texto plano

## Sintaxis HTML

Html basa su sintaxis en etiquetas

### Las etiquetas tienen dos partes

***Apertura:*** Especifica el comienzo de la etiqueta, contiene sus atributos.

`<etiqueta atributo1="var1" atr2="var2">`

***Cierre:*** Define el final de la etiqueta, todo lo contenido entre esta y la de apertura esta dentro del ambito de la etiqueta.

`</etiqueta>`

### Las etiquetas pueden ser:

#### Vacías:
Se utilizan para especificar elementos que no necesitan contener mas etiquetas en su interior
`<etiqueta>`

En XHTML se recomienda:
`<etiqueta/>`

#### Contenedora:
Consta de una marca de apertura y de cierre (con /)

Las etiquetas contededoras pueden contener mas etiquetas en su ambito

Las etiquetas HTML pueden contener atributos, estos son una serie de valores que permiten especificar el comportamiento de una etiqueta dada

HTML no es sensible a mayusculas o minusculas. Sin embargo, lo aconsejable es usar minusculas. Ya que otros lenguajes no son tan flexibles.

## Etiquetas:
#### **Title**: 
Define el título de la pagina web que le da nombre a la pestaña, esta etiqueta se coloca en el *HEAD*.

#### **meta**:
Define todo lo que no se ve, esta etiqueta se coloca en el *HEAD*.

*charset* :
Especifica el juego de caracteres del documento en HTML5, esta etiqueta se coloca en el *HEAD*.

*http-equiv=content-type content* :
Especifica el juego de caracteres del documento en HTML4, esta etiqueta se coloca en el *HEAD*.

*default-style* :
Especifica la hoja de estilos. 

Otras opciones de meta son: name(autor, description, generator, keywords...) y content(contenido del name(Keyword)).

#### **base**:
Define una parte de todas las rutas del documento HTML.