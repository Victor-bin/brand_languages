# Tema 1: INTRODUCION A LENGUAJES DE PROGRAMACION #

by: ***@Victor-bin***

- - -
- [Tema 1: INTRODUCION A LENGUAJES DE PROGRAMACION](#tema-1--introducion-a-lenguajes-de-programacion)
  * [1. Introducion](#1-introducion)
  * [2. Lenguajes de programacion](#2-lenguajes-de-programacion)
  * [3. Fases de elaboracion de un programa](#3-fases-de-elaboracion-de-un-programa)
  * [4. Traductores](#4-traductores)
  * [5. Ventajas y inconvenientes](#5-ventajas-y-inconvenientes)
- - -
## 1. Introducion

**Informartica**
: Ciencia que estudia el tratamiento automático de la información.

**Aplicacion informatica**
: Aquella que se basa en el tratamiento de un conjunto de informacion.

Para ello **toda aplicacion debe tener tres fases**

> Entrada -> Proceso -> Salida

- **Entrada**
: Informacion a modificar.

- **Proceso**
: Trasformacion que se aplica a la entrada.

- **Salida**
: Informacion modificada.

En el proceso de la información se realizan distintas operaciones de diferente complejidad:
- Operaciones aritméticas
:   +,  -,  \*, /
- Operaciones relacionales
:   \>, \>=, \<, \<=, ==, !=
- Control de flujo
: Condicionales, Bucles, Módulos, ...
- Primitivas de Entrada/Salida.

Segun sea la combinacion de operaciones en el proceso, la aplicacion sera de mayor *complejidad*.
La manera de especificar esas operaciones que componen una aplicacion es mediante el uso de *lenguajes de programacion*.

## 2. Lenguajes de programacion

**Lenguajes de programacion**
: Especifican el conjunto de operaciones que es posible realizar sobre un dispositivo informático.
Los distintos tipos de lenguaje son:

- **Lenguaje máquina**
  - Está **formado por 0 y 1** y determinan el conjunto de las instrucciones posibles que se puede realizar sobre un ordenador.
  - Cada **procesador contiene sus propias instrucciones** , su propio juego.
  - **No es un lenguaje portable**. Ya que cada procesador posee su propio juego de instrucciones.
- **Lenguaje ensamblador**
  - Se pasa de 0 y 1 de lenguaje máquina a caracteres alfabéticos paracidos al lenguaje natural llamados **nemónicos**.
  -  Los programas son **igual de largos que el lenguaje máquina**.
  - Son **fácilmente revisables** comparado con lenguaje máquina.
  - Son propios de cada procesador y **no son portables**.
- **Lenguajes de alto nivel**
  - Son **independientes de la aqruitectura** del ordenador subyacente.
  - Son **portables** y compatibles entre procesadores distintos.
  - Son **programas más cortos** que los programas escritos en leng. Máquina o ensamblador.
    - Al separarse de lenguaje máquina **se necesita de una herramienta externa que realice la traducción al lenguaje máquina**. Este tipo de herramientas se denominan **traductores**\(*Pueden ser compiladores, intérpretes o ensambladores*).
  > Se llama numero ciclomatico a el numero de finales posibles de un programa. **SE DEBEN PROBAR TODOS.**

## 3. Fases de elaboracion de un programa

> Analisis -> Diseño -> Codificacion -> Explotacion -> Mantienimiento

**Analisis**
: Se realiza la **toma de datos** del problema. Toma de datos con usuario y especificación de procesos, necesidades.

>**SALIDA -> ESPECIFICACION DE REQUEREMIENTOS.
>\(¿Qué debe hacer el programa?)**


**Diseño**
: Se diseña el sistema con una codificación lógica,proporcionando **soluciones concretas** a las especificaciones detectadas en la fase de análisis.

>**SALIDA -> DIAGRAMAS QUE RESUELVEN LOS REQUERIMIENTOS.
>\(¿Cómo se hace?)**


**Codificacion**
: Traducción a lenguaje de **programación de los diseños** generados previamente y pruebas unitarias y de integración del sistema.

>**SALIDA -> PROGRAMAS ESCRITOS EN LENGUAJES DE PROGRAMACIÓN QUE RESUELVEN LOS DIAGRAMAS PLANTEADOS EN EL DISEÑO.**



**Explotacion**
: **Implantación del sistema sobre entorno de ejecución real**. Adaptando los programas a pruebas reales, masivas, de estres que comprueban el perfecto funcionamiento de los componentes en un enotorno de explotación definitivo.

>**SALIDA -> PROGRAMAS ESCRITOS EN LENGUAJE DE PROGRAMACIÓN QUE SE ENCUENTRAN EJECUTÁNDOSE EN EL ENTORNO REAL.**


**Mantenimiento**
: Pasada la fase de explotación, comienza la fase de vida real de la aplicación. Se trata de subsanación y **corrección de errores**, nuevas adaptaciones, mejoras y mantenimiento operativo de la aplicación.

>**SALIDA -> PROGRAMA OPERATIVO EN ENTORNO REAL CON MEJORAS Y ADAPTACIONES NECESARIAS PARA ALARGAR LA VIDA ÚTIL DEL MISMO.**

## 4. Traductores
Aparecen por la necesidad de salvar el **salto semántico** entre el lenguaje máquina y los lenguajes de medio-alto nivel.

Existen 3 tipos:

- **Ensamblador**
  - Traduce cada instrucción de lenguaje ensamblador a lenguaje máquina.
  - Suelen proporcionan herramientas de ejecución, depuración y seguimiento.
- **Interprete**
  1. Traduce instrucción de lenguaje de alto nivel a leng. Máquina.
  2. Ejecuta la instrucción de leng. Máquina.
  3. Repetir el proceso (1-2) en todo el programa.
- **Compilador**
  - Un compilador traduce un programa completo de un lenguaje de alto nivel a lenguaje máquina y seguidamente lo ejecuta. Para ello, sigue las siguientes fases:
  - *EDICIÓN:*
    - Escritura del código fuente del programa.
    - Salida -> *Programa fuente.*
  - *COMPILACIÓN:*
    - Traducción del programa fuente a programa objeto. Formato intermedio previo al proceso de carga y ejecución del programa.
    - Salida -> *Programa objeto.*
  - *LINKADO:*
    - Unión de diferentes programas objetos.
    - Salida -> *Programa ejecutable.*
  - *EJECUCIÓN Y DEPURACIÓN:*
    - Ejecución del programa en entorno de pruebas y explotación.
    - Salida -> *Programa ejecutable depurado.*

    >Existen lenguajes compilados y interpretados como JAVA y C#

## 5. Ventajas y inconvenientes
- ***Interpretes***
  - Rápidos de programar. La curva de aprendizaje es muy suave.
  - Los programas generados no están libres de errores.
  - Los errores se producen en tiempo de ejecución.
  - Son relativamente sencillos de implementar.
  - Se necesita un intérprete en la máquina destino para poder ejecutar programa.
  - Los programas resultantes siempre se tienen que traducir antes de ejecutar y esto ralentiza la ejecución final del programa.
  - Lenguajes interpretados
: HTML, LISP, Javascript, Python...

- - -

- ***Compiladores***
  - La curva de aprendizaje es mayor.
  - os programas generados están libres de errores sintácticos y semánticos (90% de los errores).
  - Los errores de depuración aparecen en la fase de ejecución sobre un programa ejecutable revisado y limpio de errores.
  - Los programas ejecutables son 10 veces más rápidos que los interpretados (por regla general, ya que no se tienen que traducir).
  - La capacidad de depurar el programa fuente hasta conseguir una edición libre de errores en un proceso lento que existe en los compiladores.
  - Lenguajes compilados
: C, C++, C# (FrameWork), Java (JVM)
