# CSS #

by: ***@Victor-bin***

## ID's ##

- debe usarse en **unico** elemento

- se referencia con:
```css
#IdName
{
     reglas;
}
```
- css no da problemas con id repetidos pero *JS* y otros lenguajes unicamente esperan encontrar un ID, y probablemente solo actuaran una vez

## Clases ##

- pensado para multiples elementos

- se referencia con:
```css
.ClassName
{
     reglas;
}
```
